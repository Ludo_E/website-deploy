# Procédure pour déployer un site static

1. Créer un dossier "public" à la racine du dépôt
2. Ajouter, à la racine du dépot, un fichier nommé ".gitlab-ci.yml" avec le contenu suivant :
```yaml
image: alpine:latest
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - main
```
3. Accéder au site via l'URL indiquée sur la page "Deploy > Page" de Gitlab